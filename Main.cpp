#include <iostream>
#include <string>

using namespace std;

bool cont = true;
int player[2] = {2,7};
char map[8][8] = {
	{'#', '#', '#', '#', '#', '#', '#', '#'},
	{'#', 'G', ' ', 'D', '#', 'D', ' ', '#'},
	{'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
	{'#', '#', '#', ' ', '#', ' ', 'D', '#'},
	{'#', ' ', ' ', ' ', '#', ' ', ' ', '#'},
	{'#', ' ', '#', '#', '#', '#', ' ', '#'},
	{'#', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
	{'#', '#', 'S', '#', '#', '#', '#', '#'}
};

char getInput(){
	char input;
	cin >> input;
	return toupper(input);
}

void update(char input){
	if(input == 'W'){
		if(map[player[1]-1][player[0]] != '#'){
			player[1]--;
		}
	}
	else if(input == 'S'){
		if(map[player[1]+1][player[0]] != '#'){
			player[1]++;
		}
	}
	else if(input == 'D'){
		if(map[player[1]][player[0]+1] != '#'){
			player[0]++;
		}
	}
	else if(input == 'A'){
		if(map[player[1]][player[0]-1] != '#'){
			player[0]--;
		}
	}
	else if(input == 'Q'){
		cont = false;
	}
	if(map[player[1]][player[0]] == 'D'){
		cout << "You die a horrible death." << endl;
		cont = false;
	}
	if(map[player[1]][player[0]] == 'G'){
		cout << "A winner is you!" << endl;
		cont = false;
	}
}

void printMap(){
	for(int i=0; i<8; i++){
		for(int j=0; j<8; j++){
			if(i == player[1] && j == player[0] && cont){
				cout << "P ";	
			}
			else{
				cout << map[i][j] << " ";
			}
		}
		cout << endl;
	}
}

int main(){
	cout << "Welcome to GridWorld: Quantised 'Excitement'." << endl;
	cout << "Next-gen graphics and ultra-cinematic <1fps gameplay is waiting for you!" << endl;
	cout << "Controls: W/A/S/D to move, Q to quit." << endl;
	printMap();
	while(cont){
		update(getInput());
		printMap();
	}
	cout << "Thanks for playing. There probably won�t be a next time." << endl;
	return 0;
}